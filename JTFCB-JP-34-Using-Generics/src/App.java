import java.util.ArrayList;
import java.util.HashMap;

// generic class work with other class objects
// you specify those objects
// wazne jest array in generic form!


public class App {

	public static void main(String[] args) {

		// Before Java 5 (oldschool)
		// ArrayList list = new ArrayList();
		// now object that can store other object
		// list.add("apple");
		// list.add("banana");
		// list.add("orange");
		// musisz tu zrobic downcast bo jest error
		// get zwraca obiekt wiec musisz downcast
		// String fruit = (String)list.get(1);
		// System.out.println(fruit);
		// koniec oldschool
		
		
		//////Modern style////////////////
		// tak to sie robi teraz
		// to jest minimum wiedzy na temat skladni
		// wlasciwie to trzeba znac ten modern!!!!!!!!!!!!!!
		ArrayList <String> strings = new ArrayList<String>();
		
		strings.add("cat");
		strings.add("dog");
		strings.add("alligator");
		String animal = strings.get(1);
		System.out.println(animal);
		
		// przyklad where there can be more than one type
		// jezeli masz wiecej niz jeden typ
		// klikam ctrl + shift + O zeby zaimportowac hash
		HashMap <Integer, String> map = new HashMap <Integer, String>();
		
		/// Java 7 style
		// uzywam <> na koncu puste
		// przydatne kiedy masz kilka typow string, int i zagniezdzone
		// dajesz puste <> i nie musisz powtarzac pewnych wypisow
		// ArrayList<Animal> someList = new ArrayList<>();
		
		
	}

}

